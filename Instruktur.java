import java.util.ArrayList;

public class Instruktur extends Pengguna implements CanPay {
    private ArrayList<Course> courseList;
    private ArrayList<Training> trainingList;
    private boolean isVerified;
    
    public Instruktur(String nama, String tanggalLahir, String alamat, long saldo) {
        super(nama, tanggalLahir, alamat, saldo);
        this.courseList = new ArrayList<Course>();
        this.trainingList = new ArrayList<Training>();
        this.isVerified = false;
    }

    public ArrayList<Course> getCourseList() {
        return this.courseList;
    }

    public ArrayList<Training> getTrainingList() {
        return this.trainingList;
    }

    public void setCourseList(ArrayList<Course> courseList) {
        this.courseList = courseList;
    }

    public void setTrainingList(ArrayList<Training> trainingList) {
        this.trainingList = trainingList;
    }

    public boolean isVerified() {
        return this.isVerified;
    }

    public void setVerified(boolean isVerified) {
        this.isVerified = isVerified;
    }

    public String toString(){
        String courseString = "";
        for (Course course : this.courseList){
            courseString += "- " + course.getNamaCourse() + "\n";

        }
        return String.format("Nama : %s \nTanggal Lahir : %s \nAlamat : %s\nStatus : " +(isVerified?"Verified":"Not Verified") + "\nList Course :\n"+ (this.courseList.size()>0?courseString:"Belum ada course"), getNama(), getTanggalLahir(), getAlamat());
    }

    public void enrollTraining(Training training) {
        if(!this.trainingList.contains(training)){
            this.trainingList.add(training);
            training.getTrainingAdmin().add(this);
            System.out.println(training.getTrainName() + " berhasil di enroll");
        } else {
            System.out.println("Gagal Enroll : " + training.getTrainName() + " sedang di enroll");
        }
    }

    @Override
    public void pay() {
        System.out.println("Instruktur sedang melakukan pembayaran...");
    }
}
