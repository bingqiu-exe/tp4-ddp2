import java.util.ArrayList;

public class Course {
    protected Instruktur instruktur;
    protected String namaCourse;
    protected ArrayList<Murid> muridList;
    protected ArrayList<Instruktur> instrukturList;

    public Course(Instruktur instruktur, String namaCourse) {
        this.instruktur = instruktur;
        this.namaCourse = namaCourse;
        this.muridList = new ArrayList<Murid>();
        this.instruktur.getCourseList().add(this);
    }

    // public abstract int hitungCoursePoint();

    public Instruktur getInstruktur() {
        return instruktur;
    }

    public void setInstruktur(Instruktur instruktur) {
        this.instruktur = instruktur;
    }

    public String getNamaCourse() {
        return namaCourse;
    }

    public void setNamaCourse(String namaCourse) {
        this.namaCourse = namaCourse;
    }

    public ArrayList<Murid> getMuridList() {
        return this.muridList;
    }

    public void setMuridList(ArrayList<Murid> muridList) {
        this.muridList = muridList;
    }
    
    public String toString(){
        return String.format("Nama Course:  %s - Instruktur: %s", this.namaCourse, this.instruktur.getNama());
    }

    public String details(){
        String listMurid = "";
        for(Murid murid: muridList){
            listMurid += "- " + murid.getNama() + "\n";
        }
        return String.format("Nama Course : %s\nNama Instruktur: %s\nJumlah Murid: %d \nList Murid:\n"+ (muridList.size()>0?listMurid:"belum ada yang mengambil Course ini"), this.namaCourse, this.instruktur.getNama(), this.muridList.size());
    }

    public void removeMurid(Murid murid){
        this.muridList.remove(murid);
        System.out.println("Murid berhasil dihapus");
    }
}
