import java.util.ArrayList;

public class Training {
    private String trainName;
    private String admin;
    private ArrayList<Pengguna> trainAdmin;

    public Training(String trainName, int trainPrice, Admin loginPengguna) {
        this.trainName = trainName;
        this.admin = trainName;
        this.trainAdmin = new ArrayList<>();
    }
    
    public Training(Instruktur instruktur, String nextLine) {
    }

    public String getTrainName() {
        return trainName;
    }

    public String getAdmin() {
        return admin;
    }

    public ArrayList<Pengguna> getTrainingAdmin() {
        return trainAdmin;
    }

    public void trainAdmin(Pengguna admin) {
        trainAdmin.add(admin);
    }

    public boolean isAdminTraining(Pengguna admin) {
        return trainAdmin.contains(admin);
    }

    public int size() {
        return 0;
    }
}
