import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lemao {
    private ArrayList<Course> courses = new ArrayList<Course>();
    private ArrayList<Pengguna> penggunas = new ArrayList<Pengguna>();
    private Pengguna loginPengguna;
    private ArrayList<Training> training = new ArrayList<>();
    private Pengguna pengguna;
    private CourseCart courseCart;
    private int discount2;

    
    public static void main(String[] args) throws TeacherNotFoundException {
        Lemao app = new Lemao();
        System.out.println("Welcome to Lemao! Enjoy your Learning");
        app.initData();
        Scanner scanner = new Scanner(System.in);
        int choice;
        do {
            System.out.println("Menu" +
            "\n1. Login" +
            "\n2. Daftar sebagai Murid" +
            "\n3. Daftar sebagai Instruktur" +
            "\n0. Keluar");
            System.out.print("Pilih menu: ");
            choice = scanner.nextInt();
            switch (choice){
                case 1:
                    System.out.println("Masukkan nama pengguna:");
                    String user = "";
                    if(scanner.hasNextLine()){
                        scanner.nextLine();
                        user = scanner.nextLine();
                    }
                    Pengguna pengguna = app.searchPengguna(user);
                    if (pengguna != null){
                        app.loginPengguna = pengguna;
                        if (app.loginPengguna instanceof Admin){
                            app.adminMenu();
                        } else if (app.loginPengguna instanceof Instruktur){
                           app.instrukturMenu();
                        } else if (app.loginPengguna instanceof Murid){
                            app.muridMenu();
                        }
                    } else{
                        System.out.println("Pengguna tidak ditemukan");
                    }
                    break;
                case 2:
                    System.out.println("--------- Registrasi Murid  ---------");
                    System.out.println("Nama : ");
                    String nama = "";
                    if(scanner.hasNextLine()){
                        scanner.nextLine();
                        nama = scanner.nextLine();
                    }
                    System.out.println("Tanggal Lahir : ");
                    String tl = scanner.next();
                    System.out.println("Alamat:");
                    String alamat = "";
                    if(scanner.hasNextLine()){
                        scanner.nextLine();
                        alamat = scanner.nextLine();
                    }
                    if(nama.length()>0 && alamat.length()>0){
                        System.out.println("Saldo: ");
                        long saldo = (long) scanner.nextDouble();
                        Murid murid = new Murid(nama, tl, alamat, saldo);
                        app.penggunas.add(murid);
                    }
                    System.out.println("Pendaftaran berhasil silahkan login kembali");
                    break;
                case 3:
                    System.out.println("--------- Registrasi Instruktur  ---------");
                    System.out.println("Nama : ");
                    String namaInstruktur = "";
                    if(scanner.hasNextLine()){
                        scanner.nextLine();
                        namaInstruktur = scanner.nextLine();
                    }
                    System.out.println("Tanggal Lahir : ");
                    String tlins = scanner.next();
                    System.out.println("Alamat:");
                    String alamatins = "";
                    if(scanner.hasNextLine()){
                        scanner.nextLine();
                        alamatins = scanner.nextLine();
                    }
                    if(namaInstruktur.length()>0 && alamatins.length()>0){
                        System.out.println("Saldo: ");
                        long saldo = (long) scanner.nextDouble();
                        Instruktur instruktur = new Instruktur(namaInstruktur, tlins, alamatins, saldo);
                        app.penggunas.add(instruktur);
                    }
                    System.out.println("Pendaftaran berhasil silahkan login kembali");
                    break;
                case 0:
                    System.out.println("Sampai Jumpa!");
                    break;
                default:
                    System.out.println("Pilihan menu tidak valid.");
                    break;
            }
        } while (choice !=0);
        scanner.close();
    }

    public void adminMenu(){
        try (Scanner scanner = new Scanner(System.in)) {
            int choice;
            Admin admin = (Admin) this.loginPengguna;
            do {
                System.out.println("--------------Admin "+this.loginPengguna.getNama()+ " Menu------------------" +
                        "\n1. Lihat Instruktur" +
                        "\n2. Verifikasi Instruktur" +
                        "\n3. Buat Training" +
                        "\n0. Logout");
                System.out.print("Pilih menu: ");
                choice = scanner.nextInt();

                switch (choice) {
                    case 1:
                        int number= 1;
                        for (Pengguna pengguna : penggunas){
                            if(pengguna instanceof Instruktur){
                                System.out.println("-------  " + number + "   -------");
                                System.out.println(pengguna);
                                number ++;
                            }
                        }
                        break;
                    case 2:
                        System.out.println("Berikut instruktur yang belum terverifikasi");
                        int count = 0;
                        for (Pengguna pengguna : penggunas){
                            if(pengguna instanceof Instruktur){
                                Instruktur ins = (Instruktur) pengguna;
                                if(!ins.isVerified()){
                                    System.out.println("- " + ins.getNama());
                                    count++;
                                }
                            }
                        }
                        if(count >0){
                            System.out.print("Siapa yang akan anda verifikasi? ");
                            String nama = "";
                            if(scanner.hasNextLine()){
                                scanner.nextLine();
                                nama = scanner.nextLine();
                            }
                            Instruktur insVerif = (Instruktur) searchPengguna(nama);
                            if (insVerif != null){
                                admin.verifikasiInstruktur(insVerif);
                            } 
                            else {
                                System.out.println("Instruktur " + nama + " tidak ditemukan.");
                            }
                        } else {
                            System.out.println("Tidak ada instruktur yang perlu diverifikasi");
                        }
                        break;
                    case 3:
                        System.out.println("--------- Membuat Training ---------");
                        System.out.print("Nama Training: ");
                        String trainName = scanner.next();
                        System.out.print("Harga Training: ");
                        int trainPrice = scanner.nextInt();
                        training.add(new Training(trainName, trainPrice, (Admin) loginPengguna));
                        System.out.println("Training berhasil ditambahkan");
                        break;
                    default:
                        break;
                }
                
            } while (choice != 0 );
        }
    }

    public void instrukturMenu(){
        try (Scanner scanner = new Scanner(System.in)) {
            int choice;
            Instruktur instruktur = (Instruktur) this.loginPengguna;
            do {
                System.out.println("--------------Instruktur "+this.loginPengguna.getNama()+ " Menu------------------" +
                        "\n1. Buat Course" +
                        "\n2. Lihat Course Saya" +
                        "\n3. Buat Rapor Murid" +
                        "\n4. Enroll Training" +
                        "\n5. Lihat Saldo" +
                        "\n0. Logout");
                System.out.print("Pilih menu: ");
                choice = scanner.nextInt();
                try (Scanner scanner2 = new Scanner(System.in)) {
                    switch (choice) {
                        case 1:
                           System.out.println("--------- Membuat Course  ---------");
                            if(instruktur.isVerified()){
                                System.out.println("Jenis Course\n1. Course Gratis\n2. Course Berbayar");
                                System.out.print("Pilih menu: ");
                                int jenis = getUserChoice(scanner);
                                if(jenis == 1){
                                    System.out.println("Nama Course: ");
                                    if(scanner2.hasNextLine()){
                                        scanner2.nextLine();
                                        this.courses.add(new Course(instruktur, scanner2.nextLine()));
                                        System.out.println("Course berhasil ditambahkan");
                                    }
                                } 
                                if(jenis==2){
                                    System.out.println("Nama Course: ");
                                    String namaCourse ="";
                                    if(scanner2.hasNextLine()){
                                        scanner2.nextLine();
                                        namaCourse = scanner2.nextLine();
                                    }
                                    System.out.println("Harga Course: ");
                                    Long price = scanner2.nextLong();
                                    this.courses.add(new PaidCourse(instruktur, namaCourse, price));
                                    System.out.println("Course berhasil ditambahkan");
                                }
                            } else {
                                System.out.println("Nama Course: ");
                                if(scanner2.hasNextLine()){
                                    this.courses.add(new Course(instruktur, scanner2.nextLine()));
                                    System.out.println("Course berhasil ditambahkan");
                                }
                            }
                            break;
                        case 2:
                            int number = 1;
                            for(Course course : instruktur.getCourseList()){
                                System.out.println("-------  " + number + "   -------");
                                System.out.println(course.details());
                                System.out.println();
                                number++;
                            }
                            break;
                        case 3: 
                            System.out.println("Berikut merupakan list course anda: ");
                            if(instruktur.getCourseList().size()>0){
                                int no = 1;
                                for(Course course : instruktur.getCourseList()){
                                    System.out.println(no+". "+course);
                                    no++;
                                }
                                System.out.print("Masukkan nomor course: ");
                                int nomor = scanner.nextInt();
                                Course courseChoosen = instruktur.getCourseList().get(nomor-1);
                                System.out.println("\n-----------------------------\nBerikut merupakan murid yang berada dikelas ini");
                                int noStudent = 1;
                                for(Murid murid : courseChoosen.getMuridList()){
                                    System.out.println(noStudent+". "+murid.getNama());
                                    noStudent++;
                                }
                                System.out.print("Masukkan nomor murid: ");
                                int noChoosen = scanner.nextInt();
                                Murid muridChoosen = courseChoosen.getMuridList().get(noChoosen-1);
                                System.out.print(String.format("\nNilai untuk %s pada %s: ", muridChoosen.getNama(), courseChoosen.getNamaCourse()));
                                int nilai = scanner.nextInt();

                                System.out.println("Feedback untuk murid: ");
                                String feedback ="";
                                if(scanner2.hasNextLine()){
                                    // scanner2.nextLine();
                                    feedback = scanner2.nextLine();
                                }

                                ReportCard report = new ReportCard(courseChoosen, muridChoosen, nilai, feedback);
                                muridChoosen.addReportCard(report);
                                courseChoosen.removeMurid(muridChoosen);

                            } else {
                                System.out.println("Tidak ada Course Aktif");
                            }
                            break;
                        case 4:
                            System.out.println("Berikut daftar Training yang ditawarkan pada Lemao:");
                            for (int i = 0; i < this.training.size(); i++){
                                System.out.println(i+1 + ". " + this.training.get(i));
                            }
                            System.out.print("Masukkan nomor course: ");
                            int nomor = scanner.nextInt();
                            instruktur.enrollTraining(this.training.get(nomor-1));
                            System.out.println("\n-------------- Payment ------------------");
                            System.out.println("Metode pembayaran:" +
                                    "\n1. Course Gratis" +
                                    "\n2. Course Berbayar");
                            System.out.print("Pilih metode pembayaran: ");
                            int jenis;
                            while (true) {
                                try {
                                    jenis = scanner2.nextInt();
                                    break;
                                } catch (java.util.InputMismatchException e) {
                                    System.out.println("Invalid input. Please enter a valid number.");
                                    scanner2.nextLine();
                                }
                            }
                            if(jenis == 1){
                                System.out.println("—----- Payment Berhasil —----");
                                System.out.println(this.training + " berhasil dienroll");
                            }
                            if(jenis==2){
                                System.out.print("Masukkan nomor credit card anda: ");
                                if (scanner2.hasNextLine()) {
                                    String inputNomorKartu = scanner2.nextLine();
                                    EwalletPayment ewalletPayment = new EwalletPayment(pengguna, courseCart, discount2);
                                    if (inputNomorKartu.equals(ewalletPayment.getNomorKartu())) {
                                        this.training.add(new Training(instruktur, inputNomorKartu));
                                        System.out.println(this.training + " berhasil ditambahkan");
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
                
            } while (choice != 0 );
        }
    }

    public void muridMenu(){
        try (Scanner scanner = new Scanner(System.in)) {
            int choice;
            Murid murid = (Murid) this.loginPengguna;
            do {
                System.out.println("--------------Murid "+this.loginPengguna.getNama()+ " Menu------------------" +
                        "\n1. Enroll Course" +
                        "\n2. Lihat Course Aktif Saya" +
                        "\n3. Lihat Rapor" +
                        "\n4. Bayar Course" +
                        "\n5. Lihat Saldo" +
                        "\n0. Logout");
                System.out.print("Pilih menu: ");
                choice = scanner.nextInt();
                switch (choice) {
                    case 1:
                        System.out.println("Berikut course yang ditawarkan pada Lemao:\n");
                        for (int i = 0; i < this.courses.size(); i++){
                            System.out.println(i+1 + ". " + this.courses.get(i));
                        }
                        System.out.print("Masukkan nomor course: ");
                        int nomor = scanner.nextInt();
                        murid.enrollCourse(this.courses.get(nomor-1));
                        break;
                    case 2:
                        if(murid.getCourseList().size()>0){
                            int number = 1;
                            for(Course course : murid.getCourseList()){
                                System.out.println(number+". "+course);
                                number++;
                            }
                        } else {
                            System.out.println("Tidak ada Course Aktif");
                        } 
                        break;
                    case 3:
                        murid.viewReport();
                        break;
                    case 4:
                        murid.makePayment();
                        break;
                    case 5:
                        murid.viewSaldo();
                    default:
                        break;
                }
                
            } while (choice != 0 );
        }
    }


    public void initData() throws TeacherNotFoundException {
        try (Scanner scanner = new Scanner(new File("lemao-initiation.txt"))) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine().trim();
    
                if (line.isEmpty() || !line.startsWith("INSERT INTO")) {
                    continue;
                }
    
                String tableName = line.split("\\s")[2].trim();
                Matcher matcher = Pattern.compile("\\(([^)]+)\\)").matcher(line);
                if (matcher.find()) {
                    String[] values = matcher.group(1).split(",");
                    createObjects(tableName, values);
                } else {
                    System.err.println("Tidak ada value ditemukan di line: " + line);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    public int getUserChoice(Scanner scanner) {
        while (true) {
            if (scanner.hasNextInt()) {
                return scanner.nextInt();
            } else {
                System.out.println("Input invalid.");
                scanner.next();
            }
        }
    }

    private void createObjects(String tableName, String[] values) throws TeacherNotFoundException {
        switch (tableName) {
            case "Admin":
                createAdmin(values);
                break;
            case "Instruktur":
                createInstruktur(values);
                break;
            case "Murid":
                createMurid(values);
                break;
            case "Course":
                createCourse(values);
                break;
            case "PaidCourse":
                createPaidCourse(values);
                break;
            default:
                System.err.println("Tabel tidak disupport: " + tableName);
        }
    }
    
    private void createAdmin(String[] values) {
        if (values.length >= 3) {
            String nama = values[0].trim();
            String tanggalLahir = values[1].trim();
            String alamat = values[2].trim();
            penggunas.add(new Admin(nama, tanggalLahir, alamat, 0));
        } 
        else {
            System.err.println("Jumlah saldo untuk admin tidak valid: " + Arrays.toString(values));
        }
    }

    private void createInstruktur(String[] values) {
        if (values.length >= 4) {
            String nama = values[0].trim();
            String tanggalLahir = values[1].trim();
            String alamat = values[2].trim();
            try {
                Long saldo = Long.parseLong(values[3].replaceAll("[^\\d]", "").trim());
                penggunas.add(new Instruktur(nama, tanggalLahir, alamat, saldo));
            } 
            catch (NumberFormatException e) {
                System.err.println("Jumlah saldo untuk instruktur tidak valid: " + values[3]);
            }
        } else {
            System.err.println("Jumlah saldo untuk instruktur tidak valid: " + Arrays.toString(values));
        }
    }
    
    private void createMurid(String[] values) {
        if (values.length >= 4) {
            String nama = values[0].trim();
            String tanggalLahir = values[1].trim();
            String alamat = values[2].trim();
            try {
                Long saldo = Long.parseLong(values[3].replaceAll("[^\\d]", "").trim());
                penggunas.add(new Murid(nama, tanggalLahir, alamat, saldo));
            } 
            catch (NumberFormatException e) {
                System.err.println("Jumlah saldo untuk murid tidak valid: " + values[3]);
            }
        } else {
            System.err.println("Jumlah saldo untuk murid tidak valid: " + Arrays.toString(values));
        }
    }
    
    private void createCourse(String[] values) throws TeacherNotFoundException {
        if (values.length >= 2) {
            String namaInstruktur = values[0].trim();
            String namaCourse = values[1].trim();
            Instruktur instruktur = (Instruktur) searchPengguna(namaInstruktur);
            if (instruktur != null) {
                courses.add(new Course(instruktur, namaCourse));
            }
        } 
        else {
            System.err.println("Value di course tidak valid " + Arrays.toString(values));
        }
    }
    
    private void createPaidCourse(String[] values) throws TeacherNotFoundException {
        if (values.length >= 3) {
            String namaInstruktur = values[0].trim();
            String namaCourse = values[1].trim();
            try {
                Long harga = Long.parseLong(values[2].replaceAll("[^\\d]", "").trim());
                Instruktur instruktur = (Instruktur) searchPengguna(namaInstruktur);
                if (instruktur != null) {
                    courses.add(new PaidCourse(instruktur, namaCourse, harga));
                }
            } 
            catch (NumberFormatException e) {
                System.err.println("Value harga di Paidcourse tidak valid  " + values[2]);
            }
        } else {
            System.err.println("Value harga di Paidcourse tidak valid  " + Arrays.toString(values));
        }
    }        
    
    public Course searchCourse(String courseName){
        for (Course course: courses){
            if(course.getNamaCourse().toLowerCase().equals(courseName.toLowerCase())) {
                return course;
            }
        }
        return null;
    }

    public Pengguna searchPengguna(String name){
        for (Pengguna pengguna: penggunas){
            if(pengguna.getNama().toLowerCase().equals(name.toLowerCase())){
                return pengguna;
            }
        }
        return null;
    }

    
}
