public class Pengguna {
    private String nama;
    private String tanggalLahir;
    private String alamat;
    private long saldo;
    protected int point;
    private Pengguna[] penggunas;
    
    public Pengguna(String nama, String tanggalLahir, String alamat, long saldo) {
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
        this.alamat = alamat;
        this.saldo = saldo;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public long getSaldo() {
        return saldo;
    }

    public void setSaldo(long saldo) {
        this.saldo = saldo;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String toString(){
        return String.format("Nama : %s \nTanggal Lahir : %s \nAlamat : %s", getNama(), getTanggalLahir(), getAlamat());
    }

    public Pengguna searchPengguna(String name) throws TeacherNotFoundException {
        for (Pengguna pengguna: penggunas){
            if(pengguna.getNama().equalsIgnoreCase(name)){
                return pengguna;
            }
        }
        throw new TeacherNotFoundException(name);
    }
}