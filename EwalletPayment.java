public class EwalletPayment extends Payment {
    protected String nomorKartu;

    public EwalletPayment(Pengguna pengguna, CourseCart courseCart, int discount2) {
        super(pengguna, courseCart, discount2);
    }

    public String getNomorKartu() {
        return this.nomorKartu;
    }

    public void setNomorKartu(String nomorKartu) {
        this.nomorKartu = nomorKartu;
    }
}
