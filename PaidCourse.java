public class PaidCourse extends Course{
    private long harga;


    public PaidCourse(Instruktur instruktur, String namaCourse, Long harga) {
        super(instruktur, namaCourse);
        this.harga = harga;
    }

    public String details(){
        String listMurid = "";
        for(Murid murid: muridList){
            listMurid += "- " + murid.getNama() + "\n";
        }
        return String.format("Nama Course : %s\nNama Instruktur: %s\nHarga: %d\nJumlah Murid: %d \nList Murid:\n"+ (muridList.size()>0?listMurid:"belum ada yang mengambil Course ini"), this.namaCourse, this.instruktur.getNama(), this.harga, this.muridList.size());
    }
    
    public long getPrice() {
        return harga;
    }
    
}
