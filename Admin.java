public class Admin extends Pengguna {

    public Admin(String nama, String tanggalLahir, String alamat, long saldo) {
        super(nama, tanggalLahir, alamat, saldo);
    }

    public void verifikasiInstruktur(Instruktur instruktur){
        instruktur.setVerified(true);
        System.out.println("Instruktur " + instruktur.getNama() + " berhasil diverifikasi");
    }
    
}
