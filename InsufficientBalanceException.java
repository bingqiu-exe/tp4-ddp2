public class InsufficientBalanceException extends Exception {
    public InsufficientBalanceException() {
        super(String.format("Saldo Pengguna tidak cukup"));
    }
}
