class TeacherNotFoundException extends Exception {
    public TeacherNotFoundException(String teacherName) {
        super(teacherName + " tidak ada didalam sistem");
    }
}