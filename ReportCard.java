public class ReportCard {
    private Course course;
    private Murid murid;
    private int score;
    private String feedback;
    
    public ReportCard(Course course, Murid murid, int score, String feedback) {
        this.course = course;
        this.murid = murid;
        this.score = score;
        this.feedback = feedback;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Murid getMurid() {
        return murid;
    }

    public void setMurid(Murid murid) {
        this.murid = murid;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
    
    public String toString(){
        return String.format("Nama Murid: %s\nNama Course: %s\nNilai:%d\nFeedback: %s", this.murid.getNama(), this.course.getNamaCourse(), this.score, this.feedback);
    }
    
}
