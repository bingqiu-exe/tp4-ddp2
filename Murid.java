import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Murid extends Pengguna implements CanPay {
    private ArrayList<Course> courseList;
    private ArrayList<ReportCard> reportCards;
    private CourseCart courseCart;
    private Course course;
    
    public Murid(String nama, String tanggalLahir, String alamat, long saldo) {
        super(nama, tanggalLahir, alamat, saldo);
        this.courseList = new ArrayList<Course>();
        this.reportCards = new ArrayList<ReportCard>();
        this.courseCart = new CourseCart();
        this.point = 0;
    }

    public void hitungPoint(ReportCard reportCard){
        if (reportCard.getScore()>85){
            this.point += 25;
        } else if (reportCard.getScore()>70){
            this.point += 20;
        } else if (reportCard.getScore()>55){
            this.point += 10;
        } else {
            this.point += 5;
        }

        if(reportCard.getCourse() instanceof PaidCourse){
            this.point += 10;
        }
    }

    public ArrayList<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(ArrayList<Course> courseList) {
        this.courseList = courseList;
    }

    public ArrayList<ReportCard> getReportCards() {
        return reportCards;
    }

    public void setReportCards(ArrayList<ReportCard> reportCards) {
        this.reportCards = reportCards;
    }

    public void enroll(Course course) {
        if (course instanceof PaidCourse) {
            // Add the course to the course cart
            this.courseCart.getCourseList().add(course);
            this.courseCart.setTotalHarga(this.courseCart.getTotalHarga() + ((PaidCourse) course).getPrice());
            System.out.println(course.getNamaCourse() + " berhasil ditambahkan ke dalam Course Cart");
        } 
        else {
            enrollCourse(course);
        }
    }

    public void enrollCourse(Course course){
        if(!this.courseList.contains(course)){
            this.courseList.add(course);
            course.getMuridList().add(this);
            System.out.println(course.getNamaCourse() + " berhasil di enroll");
        } else {
            System.out.println("Gagal Enroll : " + course.getNamaCourse() + " sedang di enroll");
        }
    }

    public void makePayment() {
        if (this.getSaldo() >= this.courseCart.getTotalHarga()) {
            this.setSaldo(this.getSaldo() - this.courseCart.getTotalHarga());
            for (Course course : this.courseCart.getCourseList()) {
                enroll(course);
            }
            this.courseCart.getCourseList().clear();
            this.courseCart.setTotalHarga(0);
            System.out.println("Pembayaran berhasil." + course.getNamaCourse() + " berhasil dienroll!");
        } 
        else {
            System.out.println("Pembayaran gagal. Saldo tidak cukup.");
        }
    }

    public void makePaymentWithDiscount() {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("-------------- Payment ------------------");
            System.out.println("Metode pembayaran : ");
            System.out.println("1. Cash");
            System.out.println("2. Credit Card");
            System.out.print("Pilih metode pembayaran: ");
            int choice = scanner.nextInt();

            if (choice == 1) {
                System.out.println("-------- Payment Berhasil ----");
                System.out.println("Nama : " + getNama());
                System.out.println("Metode : CASH");
                printCourseInformation();
                System.out.println("Jumlah Harga Course : Rp" + formatCurrency(courseCart.getTotalHarga()) + ",-");
                System.out.println("Jumlah Harga Bayar : Rp" + formatCurrency(applyDiscount(courseCart.getTotalHarga())) + ",-");
                processPayment();
            } 
            else if (choice == 2) {
                System.out.println("Masukkan nomor credit card anda : ");
                long creditCardNumber = scanner.nextLong();
                System.out.println("---------- Payment Berhasil ----------");
                System.out.println("Nama : " + getNama());
                System.out.println("Metode : Credit Card - " + creditCardNumber);
                printCourseInformation();
                System.out.println("Jumlah Harga Course : Rp" + formatCurrency(courseCart.getTotalHarga()) + ",-");
                System.out.println("Jumlah Harga Bayar : Rp" + formatCurrency(applyDiscount(courseCart.getTotalHarga())) + ",-");
                processPayment();
            } 
            else {
                System.out.println("Pilihan metode pembayaran tidak valid.");
            }
        }
    }

    private String formatCurrency(long amount) {
        return String.format("Rp%,d", amount).replace(",", ".");
    }

    private void printCourseInformation() {
        System.out.println("List Course :");
        for (int i = 0; i < courseCart.getCourseList().size(); i++) {
            Course course = courseCart.getCourseList().get(i);
            System.out.println((i + 1) + ". Nama Course: " + course.getNamaCourse() +
                    " - Instruktur: " + course.getInstruktur());
        }
    }
    
    private long applyDiscount(long totalHarga) {
        int discountPercentage = Math.min(point / 10, 20);  // Maximum discount is 20%
        double discountMultiplier = 1 - (discountPercentage / 100.0);
        return Math.round(totalHarga * discountMultiplier);
    }

    private void processPayment() {
        if (getSaldo() >= applyDiscount(courseCart.getTotalHarga())) {
            setSaldo(getSaldo() - applyDiscount(courseCart.getTotalHarga()));
            for (Course course : courseCart.getCourseList()) {
                enroll(course);
            }
            courseCart.getCourseList().clear();
            courseCart.setTotalHarga(0);
        } else {
            System.out.println("Pembayaran gagal. Saldo tidak cukup.");
        }
    }

    public void viewSaldo() {
        System.out.println("Saldo " + getNama() + " sebesar Rp" + formatCurrency(getSaldo()) + ",-");
    }

    public void viewReport() {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("--------- Melihat Rapor ---------");
            System.out.println("Format Rapor");
            System.out.println("1. Lihat pada console");
            System.out.println("2. Cetak dalam dokumen (.txt)");
            System.out.print("Pilih jenis course: ");
            int choice = scanner.nextInt();

            if (choice == 1) {
                // View on console
                printReport();
            } else if (choice == 2) {
                // Print to a text document
                printReportToTxt();
                System.out.println("Rapor dapat dilihat pada Rapor-" + getNama() + ".txt");
            } else {
                System.out.println("Pilihan jenis course tidak valid.");
            }
        }
    }

    private void printReportToTxt() {
        try (FileWriter writer = new FileWriter("Rapor-" + getNama() + ".txt")) {
            writer.write("---------- RAPOR ----------\n");
            writer.write("Total Point Anda : " + this.point + "\n");
            writer.write("Detail:\n");

            for (int i = 0; i < reportCards.size(); i++) {
                writer.write("----------  " + (i + 1) + "  ----------\n");
                writer.write(reportCards.get(i).toString() + "\n\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addReportCard(ReportCard reportCard){
        this.reportCards.add(reportCard);
        this.courseList.remove(reportCard.getCourse());
        this.hitungPoint(reportCard);
        System.out.println("Report Card berhasil ditambahkan, point saat ini :" + this.point);
    }

    public void printReport(){
        if(this.getReportCards().size() >0){
            System.out.println("---------- RAPOR ----------");
            System.out.println("Total Point Anda :" + this.point);
            System.out.println("Detail: ");
            for(int i = 0; i < reportCards.size(); i++){
                System.out.println("----------  " + (i+1) + "  ----------");
                System.out.println(this.getReportCards().get(i));
                System.out.println();
            }
        } else {
            System.out.println("Anda belum menyelesaikan course apapun");
        }
        
    }

    @Override
    public void pay() {
        System.out.println("Murid sedang melakukan pembayaran...");
    }
}
