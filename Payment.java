import java.util.List;

public class Payment {
    private Pengguna pengguna;
    private CourseCart courseCart;
    private long totalPembayaran;
    private int discount;

    public Payment(Pengguna pengguna, CourseCart courseCart, int discount) {
        this.pengguna = pengguna;
        this.courseCart = courseCart;
        this.discount = discount;
        calculateTotalPembayaran();
    }

    public void calculateTotalPembayaran() {
        List<Course> courses = courseCart.getCourseList();
        long totalCoursePrice = 0;

        for (Course course : courses) {
            if (course instanceof PaidCourse) {
                totalCoursePrice += ((PaidCourse) course).getPrice();
            }
        }

        totalPembayaran = totalCoursePrice - discount;
    }

    public void processPayment() {
        System.out.println("Pembayaran lagi diproses");
    }

}
